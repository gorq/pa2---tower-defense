# compiler and flags
CXX = g++
CPPFLAGS = -Wall -pedantic -Wno-long-long -O0 -ggdb

# file structure 
SRC ?= src
TOWERS ?= src/towers
PATHS ?= src/path_finders
BUILD ?= build

OBJECTS = build/main.o build/Game.o build/GameField.o build/Unit.o build/Menu.o build/BFS.o build/Tower.o build/Dijkstra.o

.PHONY: clean
clean:
	rm -rf $(BUILD)/*.*
	rm -rf ./murinmat 
	rm -rf doc/*

.PHONY: main
main: 

.PHONY: compile
compile:
	cd $(SRC); make main
	cd $(TOWERS); make main
	cd $(PATHS); make main
	$(CXX) $(CPPFLAGS) $(OBJECTS) -o murinmat -lncurses

.PHONY:run
run:
	./murinmat

.PHONY: doc
doc:
	doxygen Doxyfile

