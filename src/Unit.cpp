#include "Unit.hpp"
#include <iostream>
#include <climits>
#include <fstream>
#include <sstream>

void Unit::get_position()
{
  int x, y;
  std::cout << "Enter X and Y coor: " << std::endl;
  while (!(std::cin >> x >> y))
  {
    std::cin.clear();
    std::cin.ignore(INT_MAX, '\n');
    return get_position();
  }

  set_position(Position(x, y));
}

//===========================================================================

void Unit::read_config( std::vector<std::string>& attributes, std::vector<int*>& values, const std::string& CONFIG_FILE )
{
  using namespace std;
  ifstream file;

  file.open( CONFIG_FILE.c_str() );
  if ( ! file )
  {
    throw CONFIG_FILE + " <-- file not found";
  }
  
  // read from the file
  stringstream linefeed;
  linefeed << file.rdbuf();

  string tmp_str;
  int    tmp_int;

  // we need to be able to read all attributes in the param[0] and load values to param[1]
  for ( unsigned long i = 0; i < attributes.size(); ++i )
  {
    if ( 
      ! ( linefeed >> tmp_str >> tmp_int ) 
       || tmp_str != attributes[ i ] 
       ) 
    {
			throw "File: " + CONFIG_FILE + " in wrong format";
    }
    else if ( attributes[ i ] == "Range" && tmp_int <1 )
    {
      throw "Conf file of: " + CONFIG_FILE + " has negative range";
    }
    *values[ i ] = tmp_int;
  }
}


