#include "Menu.hpp"

int Menu::ask(int y_start, int x_start, int first_to_highlight ) const
{
  // initialize keypad and turn off cursor
  keypad( stdscr, true );
  curs_set( 0 );
  noecho();

  // initially print the menu
  for ( int i = 0; i < m_Option_count; ++i  )
    mvprintw( y_start+i, x_start, m_Items[i].c_str() );

  // set the highlight
  for ( int i = 0; i < m_Option_count; ++i )
  {
    if ( i == first_to_highlight )
      attron( A_STANDOUT );
    else
      attroff( A_STANDOUT );
    mvprintw( y_start+i, x_start, m_Items[i].c_str());
  }

  int ch = -1;
  int i = first_to_highlight;

  // get our option from user
  while ( true )
  {
    ch = getch();
    mvprintw( y_start+i, x_start, "%s", m_Items[i].c_str() );
    // arrow up
    if ( ch ==  KEY_UP )
    {
      if ( i > 0 )
        --i;
    }
    // arrow down
    else if ( ch == KEY_DOWN )
    {
      if ( i < m_Option_count-1 )
        ++i;
    }
    // enter    
    else if ( ch == '\n')
        return i;
    // user has inputted the option nummber directly
    else if ( ch >= 48 && ch < m_Option_count+48 )
      return ch-48;

    // change the highlight  
    attron( A_STANDOUT );
    mvprintw( y_start+i, x_start, "%s", m_Items[i].c_str() );
    attroff( A_STANDOUT );
  }
}
