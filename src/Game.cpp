#include "Game.hpp"
#include "GameField.hpp"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <climits>
#include <unistd.h>
#include <ncurses.h>
#include "towers/Tower.hpp"
#include "towers/Basic_tower.hpp"
#include "towers/Vaporizer_tower.hpp"
#include "towers/Pulsar_tower.hpp"
#include "towers/Bomber_tower.hpp"
#include "towers/NUKLER_tower.hpp"
#include "enemies/Slow_enemy.hpp"
#include "enemies/Fast_enemy.hpp"

// these are the constants we set / check, when we are saving / loading game
#define CONSTANT_1 1337
#define CONSTANT_2 -25
#define CONSTANT_3 300

bool Game::read_unit_configs()
{
  // try to load towers configs
  std::vector<std::string> tmp_vals;
  tmp_vals.insert( tmp_vals.end(), { "Power:", "Range:", "Cost:" } );
  std::vector<int*>        ints;

  try
  {
    /** Basic Tower conf */
    ints.insert( ints.end(), {  &Basic_tower::POWER,
                                &Basic_tower::RANGE,
                                &Basic_tower::COST } );
    Basic_tower::read_config( tmp_vals, ints, "src/configs/Basic_tower.conf" );
    ints.clear();

    /** Pulsar Tower conf */
    ints.insert( ints.end(), { &Pulsar_tower::POWER,
                               &Pulsar_tower::RANGE,
                               &Pulsar_tower::COST } );
    Pulsar_tower::read_config( tmp_vals, ints, "src/configs/Pulsar_tower.conf" );
    ints.clear();

    // add "frequency" param because of next tower types
    tmp_vals.push_back( "Frequency:" );

    /** Vaporizer Tower conf */
    ints.insert( ints.end(), { &Vaporizer_tower::POWER, &Vaporizer_tower::RANGE,
                               &Vaporizer_tower::COST,  &Vaporizer_tower::FREQUENCY } );
    Vaporizer_tower::read_config( tmp_vals, ints, "src/configs/Vaporizer_tower.conf" );
    ints.clear();
    
    /** NUKLER tower conf */
    ints.insert( ints.end(), { &NUKLER_tower::POWER, &NUKLER_tower::RANGE,
                               &NUKLER_tower::COST,  &NUKLER_tower::FREQUENCY  } );
    NUKLER_tower::read_config( tmp_vals, ints, "src/configs/NUKLER_tower.conf" );
    ints.clear();

    /** Bomber tower conf */
    ints.insert( ints.end(), { &Bomber_tower::POWER, &Bomber_tower::RANGE,
                               &Bomber_tower::COST,  &Bomber_tower::FREQUENCY  } );
    Bomber_tower::read_config( tmp_vals, ints, "src/configs/Bomber_tower.conf" );
    ints.clear();

    /** now load enemies */
    tmp_vals.clear();
    /** Fast enemy */
    tmp_vals.insert ( tmp_vals.end(), { "Health:", "Power:", "Range:", "AttackSpeed:" } );
    ints.insert( ints.end(), { &Fast_enemy::HEALTH, &Fast_enemy::POWER,
                               &Fast_enemy::RANGE,  &Fast_enemy::ATSPEED} );
    Fast_enemy::read_config( tmp_vals, ints, "src/configs/Fast_enemy.conf");
    ints.clear();

    /** Slow enemy */
    tmp_vals[3] = "Speed:";
    ints.insert( ints.end(), { &Slow_enemy::HEALTH, &Slow_enemy::POWER,
                               &Slow_enemy::RANGE,  &Slow_enemy::SPEED } );
    Slow_enemy::read_config( tmp_vals, ints, "src/configs/Slow_enemy.conf");
  }

  // catch if any of them threw an exception
  catch ( const std::string& error_message )
  {
    mvprintw( 0, 0, "Error occured while loading tower configs. Error message: %s", error_message.c_str() );
    getch();
    return false;
  }
  return true;
}

//===========================================================================

bool Game::load_game( int savefile_number, std::string& error_message )
{
  std::ifstream savefile;
  /** initialize the save file, currently just one available */
  std::string save_name = "src/saves/gamesave_";
  save_name += std::to_string(savefile_number)+".save";

  /** try to read from the save file */
  savefile.open ( save_name.c_str(), std::istream::binary );
  if ( savefile.fail() )
  {
    error_message = "Problems with opening the file";
    return 0;
  }

  savefile.seekg( 0, savefile.end );
  int size = savefile.tellg();
  savefile.seekg( 0 );

  //check if at least size is enough for initialization 
  if ( size < 8 )
    return 0;

  //allocate memory for the data
  int* data = new int[ size/sizeof(int) ];
  // read from the file
  savefile.read( (char*)data, size );

  // check our saving constants
  if ( data[ 5 ] != CONSTANT_1 || data[ 6 ] != CONSTANT_2 || data[ 7  ] != CONSTANT_3 )
  {
    delete[] data;
    error_message = "Save data corrupted";
    m_Game.reset();
    return false;
  }
  // save the data
  int map_number =  data[ 0 ];
  int castle_hp  =  data[ 1 ];
  m_Score        =  data[ 2 ];
  m_Level        =  data[ 3 ];
  m_Gold         =  data[ 4 ];

  // load the map
  m_Game.init( map_number, castle_hp );
  // check if map sizes match
  if ( m_Game.width() != data[ 8 ] || m_Game.height() != data[ 9 ] )
  {
    delete[] data;
    m_Game.reset();
    error_message = "Map for this savefile has been changed";
    return false;
  }

  // load the tower positions into a vector first,
  //    so we dont run into problems if unknown tower signature is found
  int tower_signature;
  int y_coor;
  int x_coor;
  Tower* tmp;
  std::vector<Tower*> towers;
  for ( long unsigned i = 0; i < (size/sizeof(int)-8)/3; ++i )
  {
    tower_signature = data[ 10+i*3 ];
    y_coor = data[ 11+i*3 ];
    x_coor = data[ 12+i*3 ];
    switch ( tower_signature )
    {
      case 'b':
        tmp = new Basic_tower();
        tmp->set_position( Position( x_coor, y_coor ) );
        towers.push_back( tmp );
        break;
      case 'v':
        tmp = new Vaporizer_tower();
        tmp->set_position( Position( x_coor, y_coor ) );
        towers.push_back( tmp );
        break;
      case 'p':
        tmp = new Pulsar_tower();
        tmp->set_position( Position( x_coor, y_coor ) );
        towers.push_back( tmp );
        break;
      case 'O':
        tmp = new Bomber_tower();
        tmp->set_position( Position( x_coor, y_coor ) );
        towers.push_back( tmp );
        break;
      case 'N':
        tmp = new NUKLER_tower();
        tmp->set_position( Position( x_coor, y_coor ) );
        towers.push_back( tmp );
        break;
      default: // wrong tower has been found in save file
        error_message = "Save file corrupted";
        m_Game.reset();
        for ( auto it = towers.begin(); it != towers.end(); ++it )
          delete (*it);
        delete[] data;
        return false;
    }
  }

  // place all the towers if all was a success
  for ( auto it : towers )
    m_Game.place_tower( it, it->position() );

  delete[] data;
  return true;
}

//===========================================================================

bool Game::save_game( int savefile_number )
{
  int map_number = m_Game.map_number();
  int castle_hp = m_Game.castle_health();

  /** set to which save file are we saving to */
  std::string save_file = "src/saves/gamesave_";
  save_file += std::to_string(savefile_number)+".save";

  // specify how are we going to write to file
  std::ofstream save_game ( save_file.c_str(), std::ios::binary );

  //now load towers, first 4bytes is tower, then its X coordinate, then its Y coordinate
  std::vector<Tower*> temp_towers = m_Game.get_towers();
  int size = 7 + (int)temp_towers.size()*3;

  //init the size of out save file
  int* data = new int[ size ];
  data[ 0 ] = map_number;
  data[ 1 ] = castle_hp;
  data[ 2 ] = m_Score;
  data[ 3 ] = m_Level;
  data[ 4 ] = m_Gold;
  // constants to validate file after we make the save
  data[ 5 ] = CONSTANT_1;
  data[ 6 ] = CONSTANT_2;
  data[ 7 ] = CONSTANT_3;
  data[ 8 ] = m_Game.width(); // map width
  data[ 9 ] = m_Game.height(); // map height

  // save the towers, first 4 bytes is tower type (reffered to by his char value), second is his y-coordinate, third is his x-coodrinate
  // has to be +1, we are not saving castle
  for ( unsigned long int i = 0; i < temp_towers.size()-1; ++i )
  {
    data[ 10+i*3  ] = (int)temp_towers[ i+1 ]->signature();
    data[ 11+i*3  ] = temp_towers[ i+1 ]->position().m_ycoor;
    data[ 12+i*3 ] = temp_towers[ i+1 ]->position().m_xcoor;
  }  

  //write into our file
  for ( int i = 0; i < size; ++i )
    save_game.write( (char*) &data[ i ], sizeof(int) );

  delete[] data;
  save_game.close();
  return true;
}

//===========================================================================

void Game::tower_info() const
{
  clear();
  mvprintw(0, 0, "Basic tower [%d, 'B']:", Basic_tower::COST);
  mvprintw(1, 5, "Shoots 1 enemy every step.");
  mvprintw(2, 5, "Power: %d", Basic_tower::POWER);
  mvprintw(3, 5, "Range: %d", Basic_tower::RANGE);

  mvprintw(7,  0, "Vaporizer tower [%d, 'V']:", Vaporizer_tower::COST);
  mvprintw(8,  5, "Shoots 1 enemy every 3 steps.");
  mvprintw(9,  5, "Power: %d", Vaporizer_tower::POWER);
  mvprintw(10, 5, "Range: %d", Vaporizer_tower::RANGE);

  mvprintw(14, 0, "Pulsar tower [500, 'P']:");
  mvprintw(15, 5, "Hits every enemy in range each step.");
  mvprintw(16, 5, "Power: 2");
  mvprintw(17, 5, "Range: 8");
  mvprintw(18, 5, "Armor: 5");

  mvprintw(21, 0, "Bomber tower [%d, 'O']:", Bomber_tower::COST );
  mvprintw(22, 5, "Bombards enemies for mediocre damage each %d steps", Bomber_tower::FREQUENCY );
  mvprintw(23, 5, "Power: %d", Bomber_tower::POWER );
  mvprintw(24, 5, "Range: %d", Bomber_tower::RANGE );

  mvprintw(28, 0, "NUKLER tower [%d, 'N']:", NUKLER_tower::COST );
  mvprintw(29, 5, "Bombards enemies for massive damage each %d steps", NUKLER_tower::FREQUENCY );
  mvprintw(30, 5, "Power: %d", NUKLER_tower::POWER );
  mvprintw(31, 5, "Range: %d", NUKLER_tower::RANGE );

  mvprintw(35, 0, "Press anything to return");
  move(36, 0);

  getch();
  clear();
  print();
  m_Game.print();
}

//===========================================================================

void Game::print() const
{
  int rows, cols;
  getmaxyx( stdscr, rows,cols );
  int x_start = cols/2-m_Game.width()/2-30;
  int y_start = rows/2-m_Game.height()/2-3;

  mvprintw(y_start, cols/2+m_Game.width()/2+10, "CURRENT ENEMIES:");
  mvprintw(y_start,   x_start+5, "BIG TOWER DEFENSE");
  mvprintw(y_start+1, x_start, "Castle HP  %-8d", m_Game.castle_health());
  mvprintw(y_start+2, x_start, "Score      %-8d", m_Score);
  mvprintw(y_start+3, x_start, "Level      %-8d", m_Level);
  mvprintw(y_start+4, x_start, "Money $$$  %-8d", m_Gold);
  for ( int i = -1; i < 30; ++i )
  {
    move( y_start+5, x_start+i );
    addch('_');
  }
}

void Game::print_defeat() const
{
  int rows,cols;
  getmaxyx(stdscr, rows,cols);

  int y_start = rows/2-m_Game.height()/2-6;
  int x_start = cols/2-m_Game.width()/2+1;


  mvprintw(y_start,   x_start, "/_____/_____/_/   /_____/_/  |_/_/      ");
  mvprintw(y_start-1, x_start, " / /_/ / /___/ __/ / /___/ ___ |/ /     ");
  mvprintw(y_start-2, x_start, "  / / / / __/ / /_  / __/ / /| | / /    ");
  mvprintw(y_start-3, x_start, "   / __ \\/ ____/ ____/ ____/   |/_  __/ ");
  mvprintw(y_start-4, x_start, "    ____   ____  ____  _____ __  ______ ");

  int end = 't';
  while ( end == 't')
  {
    end = getch();
  }
}

//===========================================================================

void Game::print_message_line(const std::string &message) const
{
  int rows, cols;
  getmaxyx( stdscr, rows, cols );
  int y_start = rows/2-m_Game.height()/2-3;
  int x_start = cols/2-m_Game.width()/2+1;

  mvprintw(y_start - 2, x_start, message.c_str() );
}

//===========================================================================

void Game::flush_message_line() const
{
  int rows = getmaxy(stdscr);
  move( rows/2-m_Game.height()/2-5, 0);
  clrtoeol();
}

//===========================================================================

bool Game::build_tower(Tower *unit)
{
  flush_message_line();
  if (m_Gold >= unit->cost())
  {
    Position pos = m_Game.read_coords();
    flush_message_line();
    if ( pos.m_xcoor == -1 )
    {
      return prompt_towers();
    }
    else if (m_Game.is_taken(pos))
    {
      print_message_line("Position already taken!");
      delete unit;
      return prompt_towers();
    }
    else
    {
      m_Game.place_tower(unit, pos);
      m_Gold -= unit->cost();
      print_message_line( "Placed successfully" );
      print();
      m_Game.print();
      return prompt_towers();
    }
  }
  else
  {
    delete unit;
    flush_message_line();
    print_message_line("Not enough gold!");
    return prompt_towers();
  }
}

//===========================================================================

bool Game::prompt_towers()
{
  print();
  m_Game.print_build();
  int cols, rows;
  getmaxyx( stdscr, rows, cols );
  int y_start = rows/2-m_Game.height()/2+4;
  int x_start = cols/2-m_Game.width()/2-30;


  /** undraw last line that was overwriting */
  for( int i = 0; i < 30; ++i )
  {
    move( y_start, x_start+i );
    addch(' ');
  }

  /** get option input from our menu struct */
  int i = menu_Tower_menu.ask( y_start, x_start, 1  );

  /** create a polymorphic object based on input */
  Tower* tmp;
  switch (i)
  {
    case 0:
      clear();
      return ask_user();
    case 1:
      tmp = new Basic_tower();
      return build_tower(tmp);
    case 2:
      tmp = new Vaporizer_tower();
      return build_tower(tmp);
    case 3:
      tmp = new Pulsar_tower();
      return build_tower(tmp);
    case 4:
      tmp = new Bomber_tower();
      return build_tower(tmp);
    case 5:
      tmp = new NUKLER_tower();
      return build_tower(tmp);
    case 6:
      tower_info();
      return prompt_towers();
    default:
      return false;
  }
}

//===========================================================================

bool Game::ask_user()
{
  print();
  m_Game.print();

  int cols, rows;
  getmaxyx( stdscr, rows, cols );
  int y_start = rows/2-m_Game.height()/2+4;
  int x_start = cols/2-m_Game.width()/2-30;

  /** get option input from our menu struct */
  int i = menu_Main_menu.ask( y_start, x_start, 1  );

  Position tmp;
  switch (i)
  {
    case 0:
      return false;
    case 1:
      return prompt_towers();
    case 2:
      m_Game.print_build();
      tmp = m_Game.read_coords();
      flush_message_line();
      // only try to delete if user has chosen something, else dont do anything
      if ( tmp.m_xcoor != -1 )
      {
        if ( m_Game.can_delete( tmp ) )
        {
          print_message_line("Tower removed");
          m_Gold+=50; /** PLACEHOLDER VALUE, needs to be figured out */
          m_Game.delete_tower(tmp);
        }
        else
          print_message_line("Cannot delete this object!");
      }
      m_Game.print();
      return ask_user();
    case 3:
      return true;
    case 4:
      if ( ! save_game( 1 ) )
        print_message_line("Error while trying to save the game");
      else
        print_message_line("Saved successfully");
      return ask_user();
    default:
      return false;
  }
}

//===========================================================================

void Game::start()
{
  initscr();
  start_color( );
  // set background
  assume_default_colors(COLOR_WHITE,COLOR_BLACK);

  int cols, rows;
  getmaxyx( stdscr, rows, cols );
  int y_start = rows/2-3;
  int x_start = cols/2-10;

  // READ CONFIGS OF TOWERS, end if failed
  if ( ! read_unit_configs() )
  {
    endwin();
    return;
  }

  mvprintw( rows/2-7, cols/2-7,  "DISCLAIMER ");
  mvprintw( rows/2-6, cols/2-30, "For optimal scaling, set terminal size to at least 160x50");
  mvprintw( rows/2-5, cols/2-20, "You can do this with `resize -s 50 160`");

  //ask the user if we are playing new game or loading old game
  int i = menu_Init_menu.ask( y_start, x_start, 0);
  clear();

  std::string error_message;
  int castle_hp = 500;
  if ( i == 1 )
  {
    //game load has failed
    if ( ! load_game( i, error_message ) )
    {
      clear();
      mvprintw(rows/2, cols/2-14, "Problems with loading game !" );
      mvprintw(rows/2+1, cols/2-1, "Error Message: %s", error_message.c_str());
      getch();
      // reset starting game values
      m_Level = 0;
      m_Gold = 500; 
      m_Score = 0;
      clear();
      return start();
    }
  }
  else if  ( ! m_Game.init( 0, castle_hp ) )
  {
    endwin();
    return;
  }
  m_Game.new_path();
  clear();
  print();
  m_Game.print();

  bool game_end = false;
  unsigned long step;
  Unit *tmp;

  while (ask_user())
  {
    step = 0;
    clear();
    m_Game.print();
    // loop if no valid path is found
    while ( ! m_Game.refresh_path() )
    {
      print_message_line("NO VALID PATH FOUND!!! remove some turrets");
      if ( ! ask_user() )
      {
        endwin();
        return;
      }
    }
    m_Level++;
    /** make the level play */
    while (m_Game.play(step, m_Score) || step < 5)
    {
      print();
      // end if castle died
      if (m_Game.castle_health() <= 0)
      {
        game_end = true;
        break;
      }
      // generate slow enemies 4/5 of levels
      if ( m_Level % 5 != 0 && step < m_Level % 5 + 2 + m_Level / 5)
      {
        tmp = new Slow_enemy( m_Level*2 );
        m_Game.place_enemy(tmp);
      }
      // every 5th level slow, but sturdy enemies
      else if ( m_Level % 5 == 0 && step < m_Level % 5 + 4 + m_Level / 5)
      {
        tmp = new Fast_enemy( m_Level*2 );
        m_Game.place_enemy(tmp);
      }
      ++step;
    }
    if (game_end)
    {
      print_defeat();
      break;
    }
    m_Gold += 16 * (m_Level) + 50;
  }
  endwin();
}
