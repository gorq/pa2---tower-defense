#pragma once
#include <string>
#include <ncurses.h>
#include <vector>

/**
 * structure stores a vector of options for menu
 * menu is prompted when method ask is called
 * starting coords need to be passed upon the function
 */
struct Menu
{
    int                         m_Option_count;
    std::vector<std::string>    m_Items;

    /**
     * functions prompts the user of menu stored in class
     * @param y_start specifies starting y-axis
     * @param x_start specifies startin x-axis
     * @param first_to_highlight specifies which option should be highlighted first
     * @return index of option chosen
     */
    int ask ( int y_start, int x_start, int first_to_highlight ) const;
};

