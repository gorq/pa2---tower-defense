#pragma once

#include <vector>
#include <string>
#include "Unit.hpp"
#include "towers/Tower.hpp"
#include "path_finders/BFS.hpp"
#include "path_finders/Dijkstra.hpp"
#include "Menu.hpp"
#include "ncurses.h"
#include "curses.h"

/**
 * game-field class, does everything that goes on on the game field on the screen
 * counts units, towers, their positions etc
 */ 

class GameField
{
protected:
  int                             m_MapNumber;
  std::vector<std::vector<char>>  m_Field;
  int                             m_Width,
                                  m_Height;
  Tower*                          m_Castle;
  Position                        m_Spawn_point;

  std::vector<Tower*>             m_Towers;
  std::vector<Unit*>              m_Enemies;
  BFS*                            m_Pathing;

  Menu                            menu_Choose_map;
  Menu                            menu_Choose_difficulty;

  /**
   * @param c is the char of a tile
   * @return false if a tower cannot be placed on this type of tile
   */ 
  bool is_not_valid_tile( char c )
  { return !(c == '~' || c == '#' || c == '.' || c == 'c' || c == '^'); }

  /**
   * try to load a file map file_name, throw a std::string message when something went wrong
   * @param file_name specifies the file that we are loading the map from
   * @param castle_hp will set the hp of castle
   */
  void read_from_file(const char *file_name, int castle_hp );

public:
  GameField()
          : m_Width(0),
            m_Height(0),
            m_Castle(),
            m_Spawn_point(-1,-1),
						m_Pathing( nullptr )

  {
    menu_Choose_map.m_Option_count = 6;
    menu_Choose_map.m_Items  = { "  Exit Game  ",
                                 "    Map 1    ",
                                 "    Map 2    ",
                                 "    Map 3    ",
                                 "    Map 4    ",
                                 "    Map 5    "
                                };

    menu_Choose_difficulty.m_Option_count = 2;
    menu_Choose_difficulty.m_Items = { "  EASY   ",
                                       "NIGHTMARE"
                                      };
  }

  /**
   * reset the entire data in gamefield
   */
  void reset ()
  {
    for ( unsigned long i = 0; i < m_Field.size(); ++i )
      m_Field[ i ].clear();
    m_Field.clear();
    for ( unsigned long i = 0; i < m_Towers.size(); ++i )
      delete m_Towers[ i ];
    m_Towers.clear();
    m_Castle = nullptr;
    m_Spawn_point = Position( -1, -1 );
  }


  /**
   * DESTRUCTOR
   * destroys all dynamically allocated towers and enemies
   */ 
  ~GameField()
  {
    for (auto it = m_Towers.begin(); it != m_Towers.end(); ++it)
      delete (*it);

    for (auto it = m_Enemies.begin(); it != m_Enemies.end(); ++it)
      delete (*it);

    delete m_Pathing;
  }

  /**
   * this function is called by game when we are making a save file
   * @returns the our vector of towers, so it can be saved
   */
  std::vector<Tower*> get_towers ()
  { return m_Towers; }

  /**
   * refreshes the path for enemies at given moment
   * @return false if no path available
   */
  bool refresh_path()
  { return m_Pathing->find_path(m_Field, m_Castle->position(), m_Spawn_point, m_Towers ); }

  /**
   * function is called when new map is loaded, so it prepares pathing for that map
   */ 
  void new_path()
  { m_Pathing->generate_nodes(m_Field, m_Width, m_Height, m_Castle->position(), m_Spawn_point, m_Towers ); }

  /**
   * @param pos is position we are checking
   * @return true if is taken by a tower
   */
  bool is_taken(const Position &pos) const
  { return m_Field[pos.m_ycoor][pos.m_xcoor] != '.'; }

  /**
   * places an enemy UNIT on position pos
   * @param unit is the unit we are placing
   */
  void place_enemy(Unit *unit)
  {
    unit->set_position(m_Pathing->m_Path[unit->get_step()]);
    m_Enemies.push_back(unit);
  }

  /**
   * 
   * @return width of the game field
   */
  int width() const
  { return m_Width; };

  /**
   *
   * @return height of the game field
   */
  int height() const
  { return m_Height; }

  /**
   *
   * @return castle health
   */
  int castle_health() const
  { return m_Castle->get_health(); }

  /**
   *
   * @return map number that is currently in use
   */
  int map_number() const
  { return m_MapNumber; }

  /**
   * places a tower on a given position
   * @param unit is the tower we are placing
   * @param pos is the position it is being places on
   */
  void place_tower(Tower *unit, const Position &pos);

  /**
   * deletes a tower from given position
   * @param pos is the position
   */
  void delete_tower(const Position &pos);

  /**
   * check if there is a tower on given position
   * @param pos is the position checking
   * @return true if there is a delete-able tower on Position pos
   */
  bool can_delete(const Position& pos ) const;

  /**
   * @param iteration is current step_count of enemies, some towers attack based on thus
   * @param score is in/out, increases score counter if enemy died
   * @return true as long there are enemies
   */
  bool play(unsigned long iteration, int& score);

  /**
   * prints the current game_field
   */
  void print() const;

  /**
   * prints the current game_field
   *    also shows locations, where turrets can be placed
   */
  void print_build() const;

  /**
   * reads coordinates on the gamefield from user
   * @return the position chosen
   */
  Position read_coords() const;

  /**
   * initializes the field, asks user to choose a map and difficulty
   * @param option sets the map we are loading, 0 says user has to choose
   * @param castle_hp will se the hp of the castle
   * @return true if map has been loaded successfully
   */
  bool init( int option, int castle_hp );
};
