#pragma once
#include <string>
#include "GameField.hpp"
#include "Unit.hpp"
#include <ncurses.h>
#include "Menu.hpp"

/**
 * main class of the program
 * loads the game for user, communicates with GameField to see whats going on on it
 * keeps tracks of various things that the game is decided on
 */

class Game
{
  GameField     m_Game;
  int           m_Score,
                m_Gold;
  unsigned long m_Level;

  Menu  menu_Main_menu;
  Menu  menu_Tower_menu;
  Menu  menu_Init_menu;

  /**
   * read all the tower configs needed for the game
   * @return true if all went right
   */
  bool read_unit_configs ();

  /**
   * load the game from a savefile
   * @param savefile_number says which file are we reading from
   * @param error_message in/out, if we encounter an error while trying to load the map
   * @return
   */
  bool load_game ( int savefile_number, std::string& error_message );

  /**
   * save the current state of the game
   * @param savefile_number says to which file are we writing to
   * @return  true if save has been successfull
   */
  bool save_game( int savefile_number );

  /**
   *  prints the information of towers for user
   */
  void tower_info() const;

  /**
   *  prints score, castle_hp, level, gold, and game field
   */
  void print() const;

  /**
   * prints the defeath screen to user
   */
  void print_defeat() const;

  /**
   * function prints a line at an "information" position of the game
   * @param message specifies the message to print
   */
  void print_message_line(const std::string& message) const;

  /**
   * function clears the "information" message line
   */ 
  void flush_message_line() const;


   /**
    *
    * @param unit is unit we want to input into game field
    * @return true if was successful
    */
  bool build_tower(Tower *unit);

  /**
   * @return build_tower / ask_user / or itself on error read
   */
  bool prompt_towers();

  /**
   * @return true if next level chosen, false on exit game
   */
  bool ask_user();

public:
  Game()
          : m_Game(),
            m_Score(0),
            m_Gold(300),
            m_Level(0)
  {
    menu_Init_menu.m_Items  = { 
                                "0: NEW GAME",
                                "1: LOAD GAME"
                              };
    menu_Init_menu.m_Option_count = 2;

    menu_Main_menu.m_Items = {
                              "0: EXIT GAME",
                              "1: BUILD TOWER",
                              "2: DELETE TOWER",
                              "3: NEXT LEVEL",
                              "4: SAVE GAME"
                              };
    menu_Main_menu.m_Option_count = 5;

    menu_Tower_menu.m_Items = {
                                "0: RETURN",
                                "1: Basic         ",
                                "2: Vaporizer     ",
                                "3: Pulsar        ",
                                "4: Bomber        ",
                                "5: NUKLER        ",
                                "6: TOWER DETAILS"
                              };
    menu_Tower_menu.m_Option_count = 7;
  }

  /**
   *  starts the game, runs as long as ask_user returns true
   */
  void start();
};

