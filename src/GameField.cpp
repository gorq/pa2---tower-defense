//
// Created by gorq on 4/6/19.
//

#include "GameField.hpp"
#include "towers/Tower.hpp"
#include <iostream>
#include <climits>
#include <fstream>
#include <unistd.h>

void GameField::read_from_file(const char *file_name, int castle_hp )
{
  std::ifstream file;
  file.open( file_name, std::ios::in );

  // check validity of the file
  if ( file.fail() )
    throw "Problems while trying to open file";

  file.seekg(0, file.end);
  int size = file.tellg();
  file.seekg(0, file.beg);

  // check file size and structure, we only take: 15x15 <= file_field <= 90x50
  if ( size < 240 || size > 4550 )
  {
    throw "File size too high / low";
  }

  char* data = new char[size];
  file.read(data, size);

  m_Field.resize( 50 );

  int i = 0;
  int j = 0;
  int old_width = 0;

  Tower* dummy = nullptr;
  // read the data of file and see if it's valid
  for (int k = 0; k < size; ++k)
  {
    if (data[k] == '\n')
    {
      // initialize width of first row
      if ( i == 0 )
      {
        m_Width = j;
        old_width = j;
      }
      // if old width != new width, wrong format
      if ( j != old_width || j < 14 )
      {
        delete[] data;
        throw "Map width inconsistent";
      }
      ++i;
      j = 0;
    }
    // check if map width is too high
    else if ( j > 89 )
    {
      delete[] data;
      throw "Map is too wide";
    }
    else if ( is_not_valid_tile( data[k] ) )
    {
      delete[] data;
      throw "Not valid char found on map, check readme for info";
    }
    else
    {
      // if tile is castle, initialize it
      if (data[k] == 'c')
      {
        if ( dummy != nullptr )
        {
          delete[] data;
          throw "Multiple castles found on map";
        }
        dummy = new Tower(castle_hp, 3, 'C', Position(j, i), 2, COLOR_WHITE);
      }
      // if tile is '^', thats our spawn point
      else if ( data[k] == '^' )
      {
        //set the spawn point when found
        if ( m_Spawn_point.m_xcoor == -1 )
          m_Spawn_point = Position( j, i );
        else
        {
          delete[] data;
          throw "Multiple spawn points found on map";
        }
      }
      // else check if tile is valid
      else if ( is_not_valid_tile( data[k] ) )
      {
        delete[] data;
        throw "Invalid character found on map";
      }
      m_Field[i].emplace_back( data[k] );
      ++j;
    }
  }
  // check if at least 15 or max 50 rows are in map
  if ( i < 14 || i > 49)
  {
    delete[] data;
    throw "Map has too many / little rows";
  }
  // check if both castle and spawn point have been set
  if ( dummy == nullptr || m_Spawn_point.m_xcoor == -1 ) /* -1 is initiall location, saying its not been set yet*/
  {
    delete[] data;
    throw "Castle and/or spawn point not set on the map";
  }

  // when all went good
  m_Castle = dummy;
  place_tower( dummy, dummy->position() );
  m_Height = size/old_width+1;
  m_Width = old_width;
  delete[] data;
}

//===========================================================================

void GameField::place_tower(Tower *unit, const Position &pos)
{
  init_pair(unit->signature(), unit->color(), 0 );
  unit->set_position(pos);
    m_Towers.push_back(unit);
    m_Field[unit->position().m_ycoor][unit->position().m_xcoor] = unit->signature();
}

//===========================================================================

void GameField::delete_tower(const Position &pos)
{
    for (auto it = m_Towers.begin(); it != m_Towers.end(); ++it)
    {
        if ((*it)->position() == pos)
        {
            m_Field[(*it)->position().m_ycoor][(*it)->position().m_xcoor] = '.';
            delete (*it);
            m_Towers.erase(it);
            break;
        }
    }
}

//===========================================================================

bool GameField::can_delete(const Position &pos) const
{
  return (
          m_Field[ pos.m_ycoor ][ pos.m_xcoor ] != ' ' &&
          m_Field[ pos.m_ycoor ][ pos.m_xcoor ] != '.' &&
          m_Field[ pos.m_ycoor ][ pos.m_xcoor ] != 'C'
          );
}

//===========================================================================

bool GameField::play(unsigned long iteration, int& score)
{
  int rows, cols;
  getmaxyx(stdscr, rows, cols );
  int y_start = rows/2-m_Height/2-3;
  int x_start = cols/2-m_Width/2+1;
  int  enemy_count = 0;
  bool new_enemy;
  // for each enemy on field
  for ( unsigned long i = 0; i < m_Enemies.size(); ++i )
  {
    new_enemy = false;
    enemy_count++;
    /** make towers try to attack enemy */
    for ( const auto& it : m_Towers )
    {
			//tower attacks only if it can
      if ( it->can_attack( iteration, m_Enemies[i] ) )
        it->attack( m_Enemies[i] );
			// check if enemy died
      if ( m_Enemies[i]->is_dead() )
      {
        // update score
        ++score;
        // remove his health bar
        mvprintw( y_start+enemy_count+1, x_start+ m_Width+2 , "                    " );
        // un-draw him from the field
        attron( COLOR_PAIR('.') );
        move( y_start + m_Enemies[i]->position().m_ycoor, x_start + m_Enemies[i]->position().m_xcoor );
        addch( ' ' );
        attroff( COLOR_PAIR('.' ));
        // delete him from the vector
        delete m_Enemies[i];
        m_Enemies.erase( m_Enemies.begin()+i );
        m_Enemies.shrink_to_fit();
        // set bool that enemy has died, decrease enemy count and break
        --enemy_count;
        new_enemy = true;
        break;
      }
      // check for ending condition
      if ( m_Enemies.size() == 0 )
      {
        for ( auto tow : m_Towers )
          tow->reset_status();
        return false;
      }
    }

    // go to next enemy if this one died
    if ( new_enemy )
      continue;
    
    // un-draw enemy's old position
    attron( COLOR_PAIR('.') );
    move( y_start + m_Enemies[i]->position().m_ycoor, x_start + m_Enemies[i]->position().m_xcoor );
    addch( ' ' );
    attroff( COLOR_PAIR('.' ));
    
    // see if enemy can attack Castle
    if ( m_Enemies[i]->can_attack( iteration, m_Castle ) )
      m_Enemies[i]->attack( m_Castle );
    // else update his position
      else
      {
        //update his movement
        m_Enemies[i]->next_step();
        //set his new position
        m_Enemies[i]->set_position( m_Pathing->m_Path[ m_Enemies[i]->get_step() ] );
      }

    // draw enemy's new position
    init_pair( m_Enemies[i]->signature(), m_Enemies[i]->color(), COLOR_GREEN);
    attron( COLOR_PAIR( m_Enemies[i]->signature() ) );
    move( y_start + m_Enemies[i]->position().m_ycoor, x_start + m_Enemies[i]->position().m_xcoor );
    addch( m_Enemies[i]->signature() );
    attroff( COLOR_PAIR(m_Enemies[i]->signature()) );

    /** print enemy hp only if it fits to screen */
    if( enemy_count < m_Height-2)
      mvprintw( y_start+enemy_count+1, x_start + m_Width + 2, "Enemy '%c' hp: %4d", m_Enemies[i]->signature(), m_Enemies[i]->get_health());
    refresh();
  }

  //reset tower statuses for next round
  for ( auto tower : m_Towers )
    tower->reset_status();

  usleep( 100000 );
  //undraw enemies hp from previous iteration
  for ( int i = 1; i < m_Height-2; ++i )
  {
    move( y_start+i, x_start+m_Width+2 );
    printw( "                    ");
  }
  return m_Enemies.size() > 0;
}

//===========================================================================

void GameField::print_build() const
{
  int cols, rows;
  getmaxyx( stdscr, rows, cols );
  int y_start = rows/2-m_Height/2-3;
  int x_start = cols/2-m_Width/2;

  // draws the actual game field, also shows possible tower placements
  for (int i = 0; i < m_Height - 1; ++i)
  {
    move(y_start + i, x_start);
    addch('#');
    for (int j = 1; j <= m_Width; ++j)
    {
      move(y_start + i, x_start + j);
      addch(m_Field[i][j - 1]);
    }
    move(y_start + i, x_start + m_Width + 1);
    addch('#');
  }
}

//===========================================================================

void GameField::print() const
{

  int cols, rows;
  getmaxyx( stdscr, rows, cols );
  int y_start = rows/2-m_Height/2-3;
  int x_start = cols/2-m_Width/2;

  init_pair( 2, COLOR_WHITE, COLOR_WHITE );
  attron(COLOR_PAIR(2));
  // draws top border
  for (int i = -31; i < m_Width + 31; ++i)
  {
    move(y_start - 1, x_start + i);
    addch('#');
  }
  // draws left border
  for ( int i = 0; i < m_Height+1; ++i )
  {
    move( y_start-1+i, x_start-32 );
    addch('#');
  }
  // draws right border
  for ( int i = 0; i < m_Height+1; ++i )
  {
    move( y_start-1+i, x_start+m_Width+31 );
    addch('#');
  }
  // draws bottom border
  for (int i = -31; i < m_Width + 31; ++i)
  {
    move( y_start+m_Height-1, x_start+i);
    addch('#');
  }
  attroff(COLOR_PAIR(2));

  // draws the actual game field
  for (int i = 0; i < m_Height - 1; ++i)
  {
    move(y_start + i, x_start);
    addch('#');
    for (int j = 1; j <= m_Width; ++j)
    {
      move(y_start + i, x_start + j);
      attron( COLOR_PAIR(m_Field[i][j-1]));
      addch(m_Field[i][j - 1]);
      attroff(COLOR_PAIR(m_Field[i][j-1]));
    }
    move(y_start + i, x_start + m_Width + 1);
    addch('#');
  }
}

//===========================================================================

Position GameField::read_coords() const
{
  curs_set(1);
  int cols, rows;
  getmaxyx( stdscr, rows, cols );
  int y_start = rows/2-m_Height/2-3;
  int x_start = cols/2-m_Width/2+1;
  int current_x = 0;
  int current_y = 0;

  mvprintw(y_start - 2, x_start, "Press '0' to cancel selection");
  move(y_start + current_y, x_start + current_x);

  int opt;
  keypad(stdscr, true);
  noecho();
  // read input from user via key arrows, navigate through gamefield with them
  while (true)
  {
    opt = getch();
    switch (opt)
    {
      case KEY_LEFT:
        if (current_x > 0)
          --current_x;
        else
          current_x = m_Width - 1;
        move(y_start + current_y, x_start + current_x);
        break;
      case KEY_RIGHT:
        if (current_x < m_Width - 1)
          ++current_x;
        else
          current_x = 0;
        move(y_start + current_y, x_start + current_x);
        break;
      case KEY_UP:
        if (current_y > 0)
          --current_y;
        else
          current_y = m_Height - 2;
        move(y_start + current_y, x_start + current_x);
        break;
      case KEY_DOWN:
        if (current_y < m_Height - 2)
          ++current_y;
        else
          current_y = 0;
        move(y_start + current_y, x_start + current_x);
        break;
      case '\n': // user pressed enter, we return the position of cursor on gamefield
        curs_set(0);
        return Position{current_x, current_y};
      case '0': // user has chosen to quit selection
        curs_set(0);
        return Position{-1, -1};
      default: // dont do anything
        break;
    }
  }
}

//===========================================================================

bool GameField::init( int option, int castle_hp )
{
  int cols, rows;
  getmaxyx( stdscr, rows, cols );
  // init walls as black
  init_pair( '#', 0, 0 );
  // init grass as green
  init_pair( '.', COLOR_GREEN, COLOR_GREEN );
  // init water as blue
  init_pair( '~', COLOR_BLUE, COLOR_BLUE );
  //init starting point
  init_pair( '^', COLOR_BLACK, COLOR_GREEN );

  int i = 0;
  /** get input from user via menu structure */
  if ( option == 0 )
  {
    mvprintw( rows/2-3, cols/2-8, "Choose a map:");
    i = menu_Choose_map.ask( rows/2-2, cols/2-8, 1 );
  }
  // we are loading from a save file
  else
    i = option;
  
  if ( i == 0 )
    return false;


  std::string result = "src/maps/map";
  result = result+std::to_string(i)+".map";


  // try to load a map
  std::string error_message;
  try {
    read_from_file( result.c_str(), castle_hp );
  } catch ( const char* error_message ) {
    clear();
    mvprintw(rows/2, cols/2-14, "Problems with reading map: %s", result.c_str());
    mvprintw(rows/2+1, cols/2-1, "Error Message: %s", error_message );
    getch();
    clear();
    // reset original values for next load
    delete m_Castle;
    m_Castle = nullptr;
    m_Spawn_point = Position(-1,-1);
    for ( unsigned long i = 0; i < m_Field.size(); ++i )
      m_Field[ i ].clear();
    m_Field.clear();
    return init( 0, castle_hp );
  }

  /*
  if ( ! read_from_file( result.c_str(), castle_hp, error_message ) )
  {
    clear();
    mvprintw(rows/2, cols/2-14, "Problems with reading map: %s", result.c_str());
    mvprintw(rows/2+1, cols/2-1, "Error Message: %s", error_message.c_str());
    getch();
    clear();
    return init( 0, castle_hp );
  }
  */
  // set the map number, so we save file knows which map is it supposed to load
  m_MapNumber = i;

  // ask for difficulty from the user
  clear();
  mvprintw( rows/2-3, cols/2-14, "Choose difficulty:");
  i = menu_Choose_difficulty.ask( rows/2-2, cols/2-8, 0 );
  if ( i == 0 )
    m_Pathing = new BFS();
  else
    m_Pathing = new Dijkstra();

  return true;
}
