//
// Created by gorq on 4/8/19.
//
#pragma once

#include "../Position.hpp"
#include "../towers/Tower.hpp"
#include <cmath>
#include <queue>
#include <vector>
#include <list>

/**
 * class finds the shortest path possible from start to finish in the gamefield
 */

class BFS
{
protected:
  struct Node_t
  {
    Position   m_Position;
    double     m_Distance;
    bool       m_Was_checked;
    Node_t*    m_Parent;

    Node_t(const Position& pos = Position() )
    : m_Position(pos),
      m_Distance(INFINITY),
      m_Was_checked(false),
      m_Parent(nullptr)
    {}

		bool operator < ( const Node_t& a )
		{ return m_Distance < a.m_Distance; }
  };

	class CompareNodes
	{
  public:
		bool operator () ( Node_t* a, Node_t* b )
		{ return a->m_Distance < b->m_Distance; }
	};

  std::vector<std::vector<Node_t*>>         m_Data;
  std::list<Node_t*>                        m_Queue;

  int                                       m_Data_width,
                                            m_Data_height;

  /**
   * function creates a 2D array of value-nodes and initializes them
   * @param width is the array width
   * @param height is the array height
   */
  void allocate_data(int width, int height);

  /**
   * function finds the path in maze and says if it exists
   * @param source_field is given maze
   * @param finish is our break condition
   * @return true if path has been found
   */
  bool path_exists(
                    const std::vector<std::vector<char>>& source_field,
                    const Position& finish
                    );

public:
  std::vector<Position> m_Path;
  BFS()
          : m_Data_width(0),
            m_Data_height(0)
  {}

  virtual ~BFS()
  {
    for ( auto& it : m_Data )
      for ( auto& it2 : it  )
        delete it2;
  }
  
  /**
   * function reset the data field to 0 and will call find_path to find the path
   * @param source_field is given maze
   * @param start is start point
   * @param finish is finish point
   * @return true if valid path has been found
   */
  virtual bool find_path(
                 const std::vector<std::vector<char>>& source_field,
                 const Position& start,
                 const Position& finish,
                 const std::vector<Tower*>& towers
                );

  /**
   * function generates data of nodes and finds the shortest path from start to finish
   * @param source_field is given maze
   * @param width is its width
   * @param height is its height
   * @param start is the starting position
   * @param finish is the finish position
   */
  void generate_nodes(
                      const std::vector<std::vector<char>>& source_field,
                      int width,
                      int height,
                      const Position &start,
                      const Position &finish,
                      const std::vector<Tower*>& towers
                      );
};
