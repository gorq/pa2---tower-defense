#pragma once
#include "BFS.hpp"

/**
 * class finds the shortest path possible from start to finish in the gamefield, but also makes enemies dodge towers
 */

class Dijkstra : public BFS
{
private:
    /**
     * function will increase all the fields surrounding the tower
     * @param t is the tower
     * @param value is the value by which we are increasing
     */
    void increase_field_values( Tower* t, int value );

public:
    Dijkstra()
    : BFS()
    {}

 /**
   * same as in BFS, but this one will also call increase_field_values for each tower
   * @param source_field is given maze
   * @param start is start point
   * @param finish is finish point
   * @return true if valid path has been found
   */
  bool find_path(
                 const std::vector<std::vector<char>>& source_field,
                 const Position& start,
                 const Position& finish,
                 const std::vector<Tower*>& towers
                ) override;
};
