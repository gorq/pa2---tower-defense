#include "Dijkstra.hpp"
#include <set>
//===========================================================================

void Dijkstra::increase_field_values( Tower* t, int value )
{
  std::set<Position> were_increased;
  std::queue<Position> Q;
  Q.push( Position( t->position().m_xcoor, t->position().m_ycoor ) );
  // increase value of all the fields around the tower
  Position curr;

	// create a change-coordinate vector
	std::vector<std::pair<int,int>> c_coor = { {0,1}, {0,-1}, {1,0}, {-1,0} };

  while ( !Q.empty() )
  {
    curr.m_xcoor = Q.front().m_xcoor;
    curr.m_ycoor = Q.front().m_ycoor;

    mvprintw( 0,0, "Am at x:%d y:%d", curr.m_xcoor, curr.m_ycoor );
    //getch();
    move( 0,0 );
    clrtoeol();
    were_increased.insert( Position( curr.m_xcoor, curr.m_ycoor ) );
    m_Data[ curr.m_ycoor ][ curr.m_xcoor ]->m_Distance += value;

		// for each surrouding tile add to queue if we can
		for ( auto it : c_coor )
		{
			if ( were_increased.count( Position( curr.m_xcoor+it.first, curr.m_ycoor+it.second) ) == 0
      && curr.m_xcoor-1 > 0
			&& curr.m_xcoor+1 < m_Data_width-1
			&& curr.m_ycoor-1 > 0
			&& curr.m_ycoor+1 < m_Data_height-2
      && (  abs(curr.m_xcoor+it.first - t->position().m_xcoor )
          + abs(curr.m_ycoor+it.second - t->position().m_ycoor )
          ) <= t->range()
         )
    	{
      	Q.push( Position( curr.m_xcoor+it.first, curr.m_ycoor+it.second ) );
    	}
		}
    Q.pop();
  }
}

//===========================================================================

bool Dijkstra::find_path(
                          const std::vector<std::vector<char>>& source_field,
                          const Position &start,
                          const Position &finish,
                          const std::vector<Tower*>& towers
                          )
{
	// cleanup the nodes data
  for (int i = 0; i < m_Data_height; ++i)
  {
    for (int j = 0; j < m_Data_width; ++j)
    {
      m_Data[i][j]->m_Was_checked = false;
      m_Data[i][j]->m_Distance = 0;
      m_Data[i][j]->m_Parent = nullptr;
    }
  }
  
  // for each tower on the field, we will increase the value of the fields
  //    enemies are more likely to dodge those
  for ( unsigned long i = 0; i < towers.size(); ++i )
  {
    // make surrunding of tower less likely to be stepped on
    increase_field_values( towers[ i ], towers[ i ]->range()*4 );
  }

  
  // clear the valid path vector, since we will be findinga new path
  m_Path.clear();

  m_Data[start.m_ycoor][start.m_xcoor]->m_Distance = 0;
  m_Queue.push_back(m_Data[start.m_ycoor][start.m_xcoor]);
  return path_exists(source_field, finish);
}
