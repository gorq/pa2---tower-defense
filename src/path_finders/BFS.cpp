//
// Created by gorq on 4/8/19.
//
#include "BFS.hpp"
#include <algorithm>
#include <set>

void BFS::allocate_data(int width, int height)
{
  m_Data.reserve(height+1);
  for ( int i = 0; i < height; ++i )
  {
    std::vector<Node_t*> tmp_vec;
    m_Data.emplace_back( tmp_vec );
    m_Data[i].reserve(width+1);
    {
      for ( int j = 0; j < width; ++j )
      {
        Node_t* tmp = new Node_t(Position(j,i));
        tmp->m_Distance = 0;
        m_Data[i].emplace_back( tmp );
      }
    }
  }
  m_Data_width = width;
  m_Data_height = height;
}

//===========================================================================

bool BFS::path_exists(const std::vector<std::vector<char>>& source_field, const Position& start)
{
  Node_t *current_node = nullptr;

  while (!(m_Queue.empty()))
  {
    current_node = m_Queue.front();


		// create a vector of change-coordinates
		std::vector<std::pair<int,int>> c_coor = { {0,-1}, {0,1}, {-1,0}, {1,0}, {1,1}, {1,-1},{-1,1},{-1,-1} };

    // we found the finish
    if (current_node->m_Position == start)
      break;

		// check all the surrounding nodes, add them to queue if they pass conditions
		for ( auto it : c_coor )
		{
			if ( 	 current_node->m_Position.m_xcoor > 0
					&& current_node->m_Position.m_xcoor < m_Data_width-1
					&& current_node->m_Position.m_ycoor > 0
					&& current_node->m_Position.m_ycoor < m_Data_height-2
          && !(  m_Data      [current_node->m_Position.m_ycoor+it.first][current_node->m_Position.m_xcoor+it.second]->m_Was_checked)
          && (   source_field[current_node->m_Position.m_ycoor+it.first][current_node->m_Position.m_xcoor+it.second] == '.'
              || source_field[current_node->m_Position.m_ycoor+it.first][current_node->m_Position.m_xcoor+it.second] == ' '
              || source_field[current_node->m_Position.m_ycoor+it.first][current_node->m_Position.m_xcoor+it.second] == '^'
          ))
	    {
	      m_Data[current_node->m_Position.m_ycoor+it.first][current_node->m_Position.m_xcoor+it.second]->m_Was_checked = true;
	      m_Data[current_node->m_Position.m_ycoor+it.first][current_node->m_Position.m_xcoor+it.second]->m_Parent = current_node;
	      m_Data[current_node->m_Position.m_ycoor+it.first][current_node->m_Position.m_xcoor+it.second]->m_Distance += current_node->m_Distance+1;
	      m_Queue.insert( std::upper_bound ( m_Queue.begin(),
            	                            m_Queue.end(),
          	                              m_Data[current_node->m_Position.m_ycoor+it.first][current_node->m_Position.m_xcoor+it.second],
        	                                BFS::CompareNodes() ),
      	                m_Data[current_node->m_Position.m_ycoor+it.first][current_node->m_Position.m_xcoor+it.second]);
    	}
		}
    m_Queue.pop_front();
  }

  // return false if queue has ended empty
  if (m_Queue.empty())
    return false;

  // cleanup for next iteration
  m_Queue.clear();

  // backtrack and save positions to a vector
  while (current_node)
  {
    m_Path.emplace_back(Position(current_node->m_Position));
    current_node = current_node->m_Parent;
  }

  return true;
}

//===========================================================================

bool BFS::find_path(
                          const std::vector<std::vector<char>>& source_field,
                          const Position &start,
                          const Position &finish,
                          const std::vector<Tower*>& towers
                          )
{
	// cleanup the nodes data
  for (int i = 0; i < m_Data_height; ++i)
  {
    for (int j = 0; j < m_Data_width; ++j)
    {
      m_Data[i][j]->m_Was_checked = false;
      m_Data[i][j]->m_Distance = 0;
      m_Data[i][j]->m_Parent = nullptr;
    }
  }

	// clear the valid path vector, since we will be findinga new path
  m_Path.clear();

  // we start from the finish
  m_Data[start.m_ycoor][start.m_xcoor]->m_Distance = 0;
  m_Queue.push_back(m_Data[start.m_ycoor][start.m_xcoor]);
  // we try to find start
  return path_exists(source_field, finish);
}

//===========================================================================

void BFS::generate_nodes(
                              const std::vector<std::vector<char>>& source_field,
                              int width,
                              int height,
                              const Position &start,
                              const Position &finish,
                              const std::vector<Tower*>& towers
                              )
{
  m_Data_width = width;
  m_Data_height = height;

  m_Data.clear();
  allocate_data(width, height);
  find_path(source_field, start, finish, towers );
}

