#include <iostream>
#include "Game.hpp"

int main()
{
  // initializes the game
  Game game;

  // starts the game
  game.start();
  
  return 0;
}