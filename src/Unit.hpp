#pragma once

#include "Position.hpp"
#include <ncurses.h>
#include <vector>
#include <string>

/**
 * class specifies a unit that can be placed on a game field
 */

class Unit
{
protected:
  int       m_Health,
            m_Power,
            m_Cost,
            m_Color,
            m_Stepcount;

  char      m_Signature;
  Position  m_Position;

public:
  explicit Unit(
                int health = 1,
                int power = 1,
                int cost = 1,
                char signature = ' ',
                const Position &pos = Position(-1,-1)
                )
          :
          m_Health(health),
          m_Power(power),
          m_Cost(cost),
          m_Color( COLOR_RED ),
          m_Stepcount(0),
          m_Signature(signature),
          m_Position(pos)
  {}
  virtual ~Unit() = default;

  /**
   * reads a new position of unit from user
   */
  void get_position();

  /**
   * resets status of unit, each child will have this different, all have it though
   */
  virtual void reset_status(){};

  /**
   *
   * @return color value of *this
   */
  int color () const
  { return m_Color; }

  /**
   *
   * @return current step_count of *this
   */
  virtual int get_step() const
  { return m_Stepcount; }

  /**
   * increase the step count for *this
   */
  virtual void next_step ()
  { ++ m_Stepcount; }

  /**
   * method sets the positition of &this
   * @param pos is the position
   */
  void set_position(const Position &pos)
  { m_Position = pos; }

  /**
   * *this takes damage
   * @param value is the value that *this is being attacked by
   */
  virtual void take_damage(int value)
  { m_Health -= value; }

  /**
   * *this attacks another unit
   * @param unit is the unit *this is attacking
   */
  virtual void attack(Unit *unit)
  { unit->take_damage(m_Power); }

  /**
   * @param iteration sets the current iteration of enemy movement
   * @returns true if unit can attack( each child has this different )
   */
   /**
    * this method will decide whether *this can attack or not, depending on different params in child classes
    * @param iteration is the current iteration of the game
    * @param unit is the unit *this want to attack
    * @return true *this can attack
    */
  virtual bool can_attack(unsigned long iteration, Unit *unit)
  { return true; }

  /**
   *
   * @return current position
   */
  Position position() const
  { return m_Position; }

  /** @return true if health > 0 */
  bool is_alive() const
  { return m_Health > 0; }

  /** @return true if unit health <= 0 */
  bool is_dead() const
  { return !(is_alive()); }

  /** @return current health of the unit */
  int get_health() const
  { return m_Health; }

  /** @return char_signature of the unit */
  char signature() const
  { return m_Signature; }

  /** @return cost of the unit */
  int cost() const
  { return m_Cost; }

  /**
   * function will read data from a config file, else it will throw an exception
   */
   /**
    * function will read data from a config file, else if will throw an exception
    * @param attributes is the strings we are matching values to
    * @param values is in/out vector, where method is loading the values
    * @param CONFIG_FILE is the config file where data is stored
    */
  static void read_config(
                          std::vector<std::string>& attributes,
                          std::vector<int*>& values,
                          const std::string& CONFIG_FILE
                         );
};
