#pragma once

/**
 * this is simple x and y coordinate structure
 * 	has added operator == to be able to match 2 instances
 */ 	

struct Position
{
  int     m_xcoor,
          m_ycoor;

  Position()
          : m_xcoor(0),
            m_ycoor(0)
  {}

  Position(int x, int y)
          : m_xcoor(x),
            m_ycoor(y)
  {}

  /**
   *
   * @param x is the position we are comparing
   * @return true if *this and x are equal
   */
  bool operator==(const Position& x) const
  { return m_xcoor == x.m_xcoor && m_ycoor == x.m_ycoor; }

  /**
   * @param x is the position comparing to
   * @return true if this < x
   */ 
  bool operator < ( const Position& x ) const
  {
    if ( m_ycoor != x.m_ycoor )
      return m_ycoor < x.m_ycoor;
    return m_xcoor < x.m_xcoor;
  }
};
