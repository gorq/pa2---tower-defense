#pragma once
#include "Tower.hpp"

/**
 * this tower hits harde
 * 	also hits one enemy per step
 * 	has a modify-able speed, that is read from config file
 */

class Vaporizer_tower : public Tower
{
protected:
  bool m_AttackStatus;
public:
  static int POWER,
             RANGE,
             COST,
             FREQUENCY;

  Vaporizer_tower()
          : m_AttackStatus(false)
  {
    m_Power     = POWER;
    m_Range     = RANGE;
    m_Cost      = COST;
    m_Signature = 'v';
    m_Color     = COLOR_RED;
  }

  /**
   * @param iteration is current step_count of enemies
   * @param unit is the unit we want to attack
   * @return true if can attack
   */
  bool can_attack(unsigned long iteration, Unit *unit) override
  { return (iteration % FREQUENCY == 0) && !m_AttackStatus && has_range(unit); }

  /**
   * attacks a unit, sets attack status flag to true
   * @param unit
   */
  void attack(Unit *unit) override
  {
    unit->take_damage(m_Power);
    m_AttackStatus = true;
  }

  /**
   * resets attack status flag
   */
  void reset_status() override
  { m_AttackStatus = false; }
};

int Vaporizer_tower::POWER     = 15;
int Vaporizer_tower::RANGE     = 5;
int Vaporizer_tower::COST      = 400;
int Vaporizer_tower::FREQUENCY = 2;
