#include "Tower.hpp"

/**
 * "mediocre" tower
 * hits all enemy units it has range on
 * 	is meant to hit hard, but not often
 */

class Bomber_tower : public Tower
{
protected:
  int     m_Counter;
public:
  static int POWER,
             RANGE,
             COST,
             FREQUENCY;
  Bomber_tower()
  : m_Counter( FREQUENCY )
  {
    m_Power = 50;
    m_Signature = 'O';
    m_Range = 12;
    m_Cost = 850;
    m_Color = COLOR_CYAN;
  }

  /**
   * method checks if we can currently attack
   * @param iteration is the current step of the level
   * @param unit is the unit we want to attack
   * @return true if we can
   */
  bool can_attack( unsigned long iteration, Unit* unit ) override
  { return iteration % m_Counter == 0 && has_range( unit ); }
};

int Bomber_tower::POWER     = 30;
int Bomber_tower::RANGE     = 12;
int Bomber_tower::COST      = 850;
int Bomber_tower::FREQUENCY = 5;
