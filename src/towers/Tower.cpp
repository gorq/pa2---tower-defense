#include "Tower.hpp"
#include <ncurses.h>
#include <cmath>
#include <cfloat>


bool Tower::has_range(Unit *unit) const
{
  return (abs(unit->position().m_xcoor - m_Position.m_xcoor)
				+ abs(unit->position().m_ycoor - m_Position.m_ycoor))
					<= m_Range;
}

void Tower::take_damage(int value)
{
  m_Health -= value / 5;
}
