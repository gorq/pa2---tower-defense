#include "Tower.hpp"

/**
 * this tower hits all enemy units it has range on
 */

class Pulsar_tower : public Tower
{
public:
  static int POWER,
             RANGE,
             COST;
  Pulsar_tower()
  {
    m_Power = POWER;
    m_Range = RANGE;
    m_Cost  = COST;
    m_Signature = 'p';
    m_Color = COLOR_BLUE;
  }
};

int Pulsar_tower::POWER = 10;
int Pulsar_tower::RANGE = 8;
int Pulsar_tower::COST  = 300;

