#pragma once
#include "../Unit.hpp"
#include <string>

/**
 * "main" tower class, has cone methods and members that all other towers will need
 */

class Tower : public Unit
{
protected:
  int m_Range;

  /**
   * @param unit sets the unit we are checking range for
   * @return true if we are in shooting range
   */ 
  bool has_range(Unit *unit) const;
public:
  explicit Tower(
                  int health = 1,
                  int power = 1,
                  char signature = 't',
                  const Position &pos = Position(),
                  int range = 1,
                  int color = 0
                  )
  :
    Unit( health, power, 1, signature, pos ),
    m_Range(range)
  {}

  /**
   * @return m_Range of the tower
   */
  int range () const
  { return m_Range; }

  /**
   * checks if we can attack currently
   * @param iteration is the current iteration of game level, not used in this class, but needed for nicely working polymorphism
   * @param unit is the unit we want to attack
   * @return true if we can attack
   */
  bool can_attack(unsigned long iteration, Unit *unit) override
  { return has_range(unit); }

  void take_damage(int value) override;
};


