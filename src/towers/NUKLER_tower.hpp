#include "Tower.hpp"

/**
 * "big" tower
 * hits all enemy units it has range on
 * 	is meant to hit hard, but not often
 */

class NUKLER_tower : public Tower
{
protected:
  int     m_Counter;
public:
  static int POWER,
             RANGE,
             COST,
             FREQUENCY;
  NUKLER_tower()
  : m_Counter( FREQUENCY )
  {
    m_Power     = POWER;
    m_Range     = RANGE;
    m_Cost      = COST;
    m_Signature = 'N';
    m_Color     = COLOR_MAGENTA;
  }

  /**
   * method checks if we can attack currently
   * @param iteration is the current step of the level
   * @param unit is the unit we want to attack
   * @return true if we can attack
   */
  bool can_attack( unsigned long iteration, Unit* unit ) override
  { return iteration % m_Counter == 0 && has_range( unit ); }
};

int NUKLER_tower::POWER     = 100;
int NUKLER_tower::RANGE     = 20;
int NUKLER_tower::COST      = 3000;
int NUKLER_tower::FREQUENCY = 8;
