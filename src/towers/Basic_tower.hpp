#pragma once
#include "Tower.hpp"
#include <string>

/**
 * is a basic tower
 * 	attacks 1 enemy per step
 */

class Basic_tower : public Tower
{
protected:
  int           m_AttackRate;
  bool          m_AttackStatus;
public:
  // static values to that are read during object construction
  static int POWER,
             RANGE,
             COST;

  Basic_tower()
  : m_AttackRate(1),
    m_AttackStatus(false)
  {
    // set the values
    m_Power     = POWER;
    m_Range     = RANGE;
    m_Cost      =  COST;
    m_Signature = 'b';
    m_Color     = COLOR_YELLOW;
  }

  /**
   *
   * @param iteration sets the current step count of enemies
   * @param unit is the unit we want to attack
   * @return true if can attack
   */
  bool can_attack(unsigned long iteration, Unit *unit) override
  { return !m_AttackStatus && has_range(unit); }

  /**
   * attacks the unit and sets attack status flag to true
   * @param unit is the unit we are attacking
   */
  void attack(Unit *unit) override
  {
    unit->take_damage(m_Power);
    m_AttackStatus = true;
  }

  /**
   * resets attack status flag
   */
  void reset_status() override
  { m_AttackStatus = false; }

};

int Basic_tower::POWER = 4;
int Basic_tower::RANGE = 3;
int Basic_tower::COST  = 100;


