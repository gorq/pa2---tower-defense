#!/bin/bash

# get the source code and header file names
items=$( find . -name "*.[ch]pp" )

# iterate through them and coutn via awk
for item in ${items[@]}
do 
	wc -l < $item
done | awk '
BEGIN {
	number=0
}
{
	number+=$1
}
END {
	print "Total size = " number
}
'

