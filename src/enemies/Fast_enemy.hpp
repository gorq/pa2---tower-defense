#pragma once

#include "../Unit.hpp"
#include <cmath>
#include <cfloat>

/**
 * is meant to be a fast unit, that has lower health
 */

class Fast_enemy : public Unit
{
    int m_Attack_Speed;
    int m_Attack_Counter;
    int m_Range;
public:
    static int ATSPEED,
               HEALTH,
               POWER,
               RANGE;
    explicit Fast_enemy ( int level )
        : 
          m_Attack_Speed( ATSPEED ),
          m_Attack_Counter( 0 ),
          m_Range( RANGE )
    {
        m_Signature      = '@';
        m_Stepcount      = 0;
        m_Color          = COLOR_RED;
        m_Health         = HEALTH * level / 2;
        m_Power          = POWER  * level / 5;
    }

    /**
     * @return step_count of a the unit
     */
    int get_step () const override
    { return m_Stepcount; }

    /**
     * check if *this has range on unit
     * @param unit is a unit we are checking range for
     * @returns true if we are
     */
    bool has_range ( Unit* unit ) const
    { return abs(unit->position().m_xcoor - m_Position.m_xcoor) <= m_Range &&
             abs(unit->position().m_ycoor - m_Position.m_ycoor) <= m_Range;}

    /**
     * check if it can attack, depending on its range
     * @param iteration is not used here, but is in different polymorphic objects
     * @param unit is unit we are checking if we can attack
     * @return true is in range
     */
    bool can_attack ( unsigned long iteration, Unit* unit )
    { return has_range( unit ); }

    /**
     * attack a unit
     * @param unit is the unit we are attacking
     */
    void attack ( Unit* unit ) override 
    {
        ++m_Attack_Counter;
        if ( m_Attack_Counter % m_Attack_Speed == 0 )
            unit->take_damage( m_Power );
    }
};

int Fast_enemy::ATSPEED  = 1;
int Fast_enemy::HEALTH = 10;
int Fast_enemy::POWER  = 3;
int Fast_enemy::RANGE  = 1;

