#pragma once

#include "../Unit.hpp"
#include <cmath>
#include <cfloat>

/**
 * is meant to be rather slow, but bulky unit
 */

class Slow_enemy : public Unit
{
    int        m_Speed;
    int        m_Range;
public:
    static int SPEED,
               HEALTH,
               POWER,
               RANGE;

    explicit Slow_enemy ( int level )
        : 
          m_Speed( SPEED ),
          m_Range( RANGE )
    {
        m_Signature = '&';
        m_Stepcount = 0;
        m_Color     = COLOR_BLACK;
        m_Health    = HEALTH * level / 3;
        m_Power     = POWER  * level / 5;
    }

    /** 
     * make this enemy 1/2 of a normal speed
     * @returns stepcount / speed of a unit
     */
    int get_step () const override
    { return m_Stepcount / m_Speed; }

    /**
     * check if *this has range on unit
     * @param unit specifies the unit we are checking we have range on
     * @returns true if we are
     */
    bool has_range ( Unit* unit ) const
    {
        return (abs(unit->position().m_xcoor - m_Position.m_xcoor)
				      + abs(unit->position().m_ycoor - m_Position.m_ycoor))
			        	<= m_Range;
    }

    /**
     * check if it can attack, depending on its range
     * @param iteration is not used here, but is in other unit objects
     * @param unit is the unit we are checking range for
     * @returns true if we are in range
     */
    bool can_attack ( unsigned long iteration, Unit* unit )
    { return has_range( unit); }
};

int Slow_enemy::SPEED  = 2;
int Slow_enemy::HEALTH = 20;
int Slow_enemy::POWER  = 5;
int Slow_enemy::RANGE  = 3;


